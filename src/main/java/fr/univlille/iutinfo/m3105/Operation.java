package fr.univlille.iutinfo.m3105;

public class Operation extends ExpressionTree {
	protected Operator operator;
	protected ExpressionTree left, right;
	
	public Operation(Operator op, ExpressionTree l, ExpressionTree r) {
		operator = op;
		left = l;
		right = r;
	}
	
	public Operation(Operator op, double l, ExpressionTree r) {
		this(op, new SingleValue(l), r);
	}
	
	public Operation(Operator op, ExpressionTree l, double r) {
		this(op, l, new SingleValue(r));
	}
	
	public Operation(Operator op, double l, double r) {
		this(op, new SingleValue(l), new SingleValue(r));
	}
	
	@Override
	public Double evaluate() {
		return operation.evaluate(left.evaluate(), right.evaluate());
	}
}
