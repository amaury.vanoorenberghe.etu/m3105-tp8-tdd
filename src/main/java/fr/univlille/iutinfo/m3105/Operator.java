package fr.univlille.iutinfo.m3105;

import java.util.Arrays;
import java.util.function.BiFunction;

public enum Operator {
	NONE	(null, (a, b) -> a    ),
	PLUS	("+" , (a, b) -> a + b),
	MINUS	("-" , (a, b) -> a - b),
	MULTIPLY("*" , (a, b) -> a * b);
	
	private final String NOTATION;
	private final BiFunction<Double, Double, Double> EVAL_FUNC;
	
	private Operator(String symbol, BiFunction<Double, Double, Double> evalFunc) {
		NOTATION = symbol;
		EVAL_FUNC = evalFunc;
	}
	
	public static Operator fromSymbol(String symbol) {
		return Arrays
		.stream(values())
		.filter(item -> item.NOTATION.compareTo(symbol) == 0)
		.findFirst()
		.orElse(NONE);
	}
	
	public Double evaluate(Double a, Double b) {
		return EVAL_FUNC.apply(a, b);
	}
	
	public String getSymbol() {
		return NOTATION;
	}
}
