package fr.univlille.iutinfo.m3105;

import java.util.Arrays;
import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.regex.Pattern;

/**
 * Arbre de syntaxe abstraite décrivant un calcul
 */
public abstract class ExpressionTree {
	public static Pattern TOKEN_LIMITS = Pattern.compile("\s+");
	
	protected Operator operation;
	protected ExpressionTree left, right;
	
	protected ExpressionTree() {}
	
	public ExpressionTree(double value) {
		left = new SingleValue(value);
	}
	
	public static ExpressionTree parse(String formula) {
		if (formula == null) {
			throw new NullPointerException();
		}
		
		final String[] tokens = getFormulaTokens(formula);
		
		if (tokens.length == 0) {
			throw new NoSuchElementException();
		}
		
		return null;
	}

	protected static String[] getFormulaTokens(String formula) {
		return Arrays
		.stream(TOKEN_LIMITS.split(formula))
		.filter(item -> item != null && item.length() > 0)
		.toArray(String[]::new);
	}
	
	public abstract Double evaluate();

	@Override
	public int hashCode() {
		return Objects.hash(left.hashCode(), operation.hashCode(), right.hashCode());
	}

	@Override
	public boolean equals(Object other) {
		if (this == other) return true;
		if (null == other) return false;
		if (!(other instanceof ExpressionTree)) return false;
		return equals((ExpressionTree)other);
	}
	
	public boolean equals(ExpressionTree other) {
		return Objects.equals(left, other.left)
			&& Objects.equals(operation, other.operation)
			&& Objects.equals(right, other.right);
	}
}
