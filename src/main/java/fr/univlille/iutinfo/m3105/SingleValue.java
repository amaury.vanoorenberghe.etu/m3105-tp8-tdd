package fr.univlille.iutinfo.m3105;

public class SingleValue extends ExpressionTree {
	protected final Double VALUE;
	
	public SingleValue(double value) {
		VALUE = value;
	}
	
	@Override
	public Double evaluate() {
		return VALUE;
	}
}