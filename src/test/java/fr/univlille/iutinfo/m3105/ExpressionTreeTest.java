package fr.univlille.iutinfo.m3105;

import static org.junit.jupiter.api.Assertions.*;

import java.util.NoSuchElementException;
import java.util.Random;

import org.junit.jupiter.api.Test;

public class ExpressionTreeTest {
	public static final Random RNG = new Random();
	
	@Test
	protected void should_throw_exception_if_formula_is_null() {
		assertThrows(NullPointerException.class, () -> {
			ExpressionTree.parse(null);
		});
	}
	
	@Test
	protected void should_throw_exception_if_formula_is_empty() {
		assertThrows(NoSuchElementException.class, () -> {
			ExpressionTree.parse("");
		});
	}
	
	@Test
	protected void should_throw_exception_if_formula_is_only_spaces() {
		assertThrows(NoSuchElementException.class, () -> {
			ExpressionTree.parse("     ");
		});
	}
	
	/*@Test
	protected void evaluate_a_single_value_gives_same_value() {
		ExpressionTree expr = ExpressionTree.parse("42");
		assertEquals(42, expr.evaluate());
	}
	
	@Test
	protected void evaluate_single_addition() {
		ExpressionTree expr = ExpressionTree.parse("5 + 3");
		assertEquals(8, expr.evaluate());
	}
	
	@Test
	protected void evaluate_single_subtraction() {
		ExpressionTree expr = ExpressionTree.parse("100 - 42");
		assertEquals(58, expr.evaluate());
	}
	
	@Test
	protected void evaluate_single_multiplication() {
		ExpressionTree expr = ExpressionTree.parse("8 * 8");
		assertEquals(64, expr.evaluate());
	}
	
	@Test
	protected void evaluate_operations_of_same_precedence() {
		ExpressionTree expr = ExpressionTree.parse("5 + 8 - 9");
		assertEquals(4, expr.evaluate());
	}*/
	
	@Test
	protected void expression_tree_of_single_value() {
		ExpressionTree expr = ExpressionTree.parse("17");
		ExpressionTree expected = new SingleValue(17);
		assertEquals(expected, expr);
	}
	
	/*@Test
	protected void expression_tree_of_single_addition() {
		ExpressionTree expr = ExpressionTree.parse("17 + 89");
		assertEquals(new ExpressionTree(Operation.PLUS, 17, 89), expr);
	}
	
	@Test
	protected void expression_tree_of_single_subtraction() {
		ExpressionTree expr = ExpressionTree.parse("20 - 21");
		assertEquals(new ExpressionTree(Operation.MINUS, 20, 21), expr);
	}
	
	@Test
	protected void expression_tree_of_single_multiplication() {
		ExpressionTree expr = ExpressionTree.parse("5 * 9");
		assertEquals(new ExpressionTree(Operation.MULTIPLY, 5, 9), expr);
	}*/
}